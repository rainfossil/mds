## Windows下nginx的日志分隔 ##

### 在nginx服务器根目录下创建3个bat脚本startup.bat，stop.bat，logsplit.bat ###
#### startup.bat用于启动nginx服务器，代码如下： ####
    @echo off
    rem 如果启动前已经启动nginx并记录下pid文件，会kill指定进程
    nginx.exe -s stop
    
    rem 测试配置文件语法正确性
    nginx.exe -t -c conf/nginx.conf
    
    rem 显示版本信息
    nginx.exe -v
    
    rem 按照指定配置去启动nginx
    nginx.exe -c conf/nginx.conf
    ::这里需要加上exit
    exit
#### stop.bat用于停止nginx服务器，代码如下： ####
    @echo off
    rem 如果启动前已经启动nginx并记录下pid文件，会kill指定进程
    nginx.exe -s stop
#### logsplit.bat用于分隔日志，代码如下： ####
    for /f "tokens=1 delims=/ " %%j in ("%date%") do set d1=%%j
    for /f "tokens=2 delims=/ " %%j in ("%date%") do set d2=%%j
    for /f "tokens=3 delims=/ " %%j in ("%date%") do set d3=%%j
    ::日志备份地址  
    set backupdir=d:\backup\%d1%\%d2%\%d3%  
    ::nginx安装根目录
    set basedir=C:\nginx\nginx-1.11.10
    ::备份目录已存在时直接退出
    if exist %backupdir%( 
    exit
    )
    mkdir %backupdir%  
    call %basedir%\stop.bat
    echo "%date% stop nginx"
    move %basedir%\logs\info.log %backupdir%  
    
    start %basedir%\startup.bat
    echo "%date% start nginx"
    exit
logsplit.bat将date按/符号分割为年月日的3个字符串，并在D盘的backup目录下创建年\月\日的目录，然后将对应应用的所有匹配的访问日志都移动到新创建的目录下，最后调用startup.bat脚本。

**注意：%date%的分割，未必一定是/符号，不同的windows系统上可能为不同的分隔符，有的表示为2014/05/04，有的则表示为2014-05-04的格式，所以要视实际情况来使用对应的分隔符**
### 接下来在window中添加计划任务 ###
#### 打开创建定时任务如下： ####
1. 附件-->系统工具-->任务计划程序(或者直接运行taskschd.msc)
2. 选中任务计划程序库-->创建基本任务
3. 然后就是一直设置下一步就可以了（将目标程序指定为logsplit.bat）
 
**注意：bat脚本中的路径都使用绝对路径，避免分配到计划任务的时候执行失败**

以上文档主要参考该博客进行的整理：[http://fableking.iteye.com/blog/2059691](http://fableking.iteye.com/blog/2059691 "天涯书塾的博客")
